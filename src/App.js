import React, {useState} from 'react';
import './App.scss';
import ModalWindow from './components/ModalWindow/ModalWindow';
import PageBtn from './components/PageBtn/PageBtn';
import './components/PageBtn/PageBtn.scss';

function App() {
    const appElement  =  React.createRef();


    const [state, setState] = useState({
        isBtnsShown: true,
        isModal1Shown: false,
        isModal2Shown: false
    });

    const getAppClassName = () => {
        if (state.isBtnsShown) {
            return "App"
        }
        return "App dark-bg";
    }


    const exitWindow = (e) => {

        if ( e.target.className === 'App dark-bg' ){
            setState({
                isBtnsShown: true,
                isModal1Shown: false,
                isModal2Shown: false
            })
        }
    };

    const windowOpener = (e) => {
        if (e.target.textContent === "first") {
            setState({
                isBtnsShown: false,
                isModal1Shown: true,
                isModal2Shown: false
            })
        } else {
            setState({
                isBtnsShown: false,
                isModal1Shown: false,
                isModal2Shown: true
            })
        }
    };


    const windowButtons1 = [<PageBtn btnClass={'passed-buttons1'}
                                     style={"red"}
                                     text={"Ok"}
                                     btnHandler={exitWindow}/>,
        < PageBtn
                  btnClass={'passed-buttons1'}
                  style={"red"}
                  text={"Cancel"}
                  btnHandler={exitWindow}/>];


    const windowButtons2 = [<PageBtn fontColor = { "black"}
                                     style={"#ffa31a"}
                                     text={"Ok"}
                                     btnHandler={exitWindow}/>,
        < PageBtn fontColor = { "black"}
                  style={"#ffa31a"}
                  text={"Cancel"}
                  btnHandler={windowOpener}/>,
        < PageBtn fontColor = { "black"}
                  style={"#ffa31a"}
                  text={"Exit"}
                  btnHandler={exitWindow}/>];


    return (

        <div ref={appElement} className={ getAppClassName()} onClick={exitWindow}>

                {state.isBtnsShown ?
                    <div><PageBtn btnClass={'passed-buttons'}
                                  style={"red"}
                                  text={"first"}
                                  btnHandler={windowOpener}/>
                        <PageBtn btnClass={'passed-buttons'}
                                 style={ "blue"}
                                 text={"second"}
                                 btnHandler={windowOpener}/></div>
                    : (state.isModal1Shown ?
                        <ModalWindow
                            header={"Do you want to delete this file ?"}
                            text={"Once you delete this file, it won't be possible to undo this " +
                            "action \n are you sure you want to delete this file "}
                            closeButton={true}

                            actions={windowButtons1}/>
                        : <ModalWindow
                            header={"Do you want to learn React hooks ?"}
                            text={"You might be wondering why we’re using a counter here instead of a more realistic example."}
                            closeButton={false}
                            actions={windowButtons2}/>)}
            </div>

    );
}

export default App;
