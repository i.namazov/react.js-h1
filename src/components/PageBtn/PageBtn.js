import React from 'react';
import './PageBtn.scss'
import PropTypes from "prop-types"

const PageBtn = (props) => {

    return (
        <button  className={"buttons"} style={ {backgroundColor:props.style , color: props.fontColor}} onClick={props.btnHandler}>
            {props.text}
        </button>
    );
};

PageBtn.propTypes = {
    text: PropTypes.string,
    style: PropTypes.string,
    btnHandler: PropTypes.func,
    fontColor: PropTypes.string
};

export default PageBtn;